package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("provider", is(notNullValue())));
        restAssuredThat(response -> response.body("title", is(notNullValue())));
        // the number of items may change, so we cannot rely on this assertion
        // restAssuredThat(response -> response.body("size()", is(60)));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("provider", is(notNullValue())));
        restAssuredThat(response -> response.body("title", is(notNullValue())));
        // the number of items may change, so we cannot rely on this assertion
        // restAssuredThat(response -> response.body("size()", is(115)));
    }

    @Then("he sees the results displayed for tofu")
    public void heSeesTheResultsDisplayedForTofu() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("provider", is(notNullValue())));
        restAssuredThat(response -> response.body("title", is(notNullValue())));
        // the number of items may change, so we cannot rely on this assertion
        // restAssuredThat(response -> response.body("size()", is(23)));
    }

    @Then("he sees the results displayed for water")
    public void heSeesTheResultsDisplayedForWater() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("provider", is(notNullValue())));
        restAssuredThat(response -> response.body("title", is(notNullValue())));
        // the number of items may change (happened a few times), so we cannot rely on this assertion
        // restAssuredThat(response -> response.body("size()", is(160)));
    }

    @Then("he does not see the results")
    public void he_Does_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error", is(true)));
        restAssuredThat(response -> response.body("detail.message", is("Not found")));
    }
}
