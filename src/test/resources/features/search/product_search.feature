Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario: Get each of the products
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then he sees the results displayed for tofu
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
    Then he sees the results displayed for water

  Scenario: Get invalid or non-existing products
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he does not see the results
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/appl"
    Then he does not see the results
