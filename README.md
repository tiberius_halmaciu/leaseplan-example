mvn cleanRunning the tests:
- To run the tests, run the following command: "mvn clean test serenity:aggregate"
NOTE: The test results will be generated in /target/site/serenity/index.html

How to write a new test:
1. Create a new feature file in resources/features
2. Write your desired scenarios using Gherkin format
3. Create the corresponding Step Definitions class in src/test/java/starter/stepdefinitions
4. Create a separate TestRunner class or leave it to be included with all the other tests

What was refactored:
- post_product.feature - line 13 and  SearchStepDefinitions.java - line 32 - typo "doesn"
- CarsAPI class was removed (unused)
- post_product.feature - was renamed to product_search.feature
- removed gradle related files and folders
- removed the history folder
- removed the . folders
- cleaned up serenity.conf (removed browser related info)
- removed the assertion for a particular word in the title, as they are not consistent
- removed README.md from src/main/java/starter
- changed serenity.project.name in the serenity.properties file
- some cleaning could possibly be made in the pom.xml - I did not change anything at this point
- NOTE 1: You could have multiple Test classes if you prefer to run subsets of tests
- NOTE 2: I did not rephrase the lines in the .feature file, but a discussion can be had about how much information we want to include in the Gherkin sentences
- NOTE 3: Models and various properties can be kept in src/main/java. I did not go into such depth at this point.
- NOTE 4: I kept the "starter" folder, as it may be a project convention
- NOTE 5: More in depth assertions can be made if needed - a discussion to decide a standard would be needed for this
